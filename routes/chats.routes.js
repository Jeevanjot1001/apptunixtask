const Chat = require('../models/chat.model')
const express = require('express');
const mongoose = require('mongoose');
const Room = require('../models/room.model');
const User = require('../models/user.model');
const router = express.Router();

router.get('/chats', async (req, res) => {

    const chats = await Chat.find();
    return res.status(200).json(chats)

})

router.post('/chat', async (req, res) => {
    await Chat.create({

        message: req.body.message,
        sender_id: req.body.sender_id,
        room_id: req.body.room_id,
        room_type: req.body.room_type

    }).then((result) => {
        return res.status(200).json({
            message: "Message sent",
            result: result
        });
    }).catch((err) => {
        if (err) return res.status(400).json({ err: "bad_request", error_description: err })
    })
})


// http://localhost:3000/api/userChats/?id=
router.get('/userChats', async (req, res) => {
    console.log(req.query.id)
    const myId = req.query.id;
    if (!myId) return res.status(400).json({ err: "bad_request", error_description: "Please provide ID" })

    await Room.aggregate([
        {
            $match: {
                room_members_id: {
                    $in: [mongoose.Types.ObjectId(myId)]
                }
            }
        },
        {

            $lookup: {

                from: 'chats',
                localField: '_id',
                foreignField: 'room_id',
                as: 'chatInfo'

            }
        },
        {
            $unwind: '$chatInfo'
        },
        {
            $lookup: {

                from: 'users',
                localField: 'room_members_id',
                foreignField: '_id',
                as: 'userInfo'

            }
        },
        {
            $unwind: '$userInfo'
        },
        {
            $group: {

                _id: "$chatInfo.room_id",

                chatDetails: { $last: "$chatInfo" },
                userDetails: { $push: '$userInfo' },

                unreadCount: {
                    $sum: {
                        $cond: [
                            {
                                $and:
                                    [

                                        { $in: [myId, "$chatInfo.is_seen_by"] },

                                        { $ne: [mongoose.Types.ObjectId(myId), "$chatInfo.sender_id"] }

                                    ]
                            }, 0, 1]
                    }
                }
                
            },

        },
        {
            $project: {

                unreadCount: 1,
                chatDetails: 1,
                userDetails: 1,

            }
        }
    ], (err, data) => {
        if (err) {
            return res.status(400).json({ err: "bad_request", error_description: err })
        }
        else {
            return res.status(200).json({
                message: "Message List",
                result: data
            });
        }
    });
})

module.exports = router
