const Room=require('../models/room.model')
const express = require('express');
const router = express.Router();

router.get('/rooms',async(req,res)=>{
    const rooms = await Room.find();
    return res.status(200).json(rooms)
})

router.post('/room',async(req,res)=>{
    await Room.create({

        room_name: req.body.room_name,
        room_type: req.body.room_type,
        room_image: req.body.room_image,
        room_admin_id: req.body.room_admin_id,
        room_members_id: req.body.room_members_id,
        member_count: req.body.member_count
        
    }).then((result) => {
        return res.status(200).json({
            message: "Room created",
            result: result
        });
    }).catch((err) => {
        if (err)  return res.status(400).json({ err: "bad_request", error_description: err })
    })
})

module.exports=router