const User = require('../models/user.model')
const express = require('express');
const router = express.Router();

router.get('/users', async (req, res) => {
    const users = await User.find();
    return res.status(200).json(users)
})

router.get('/user/:id', async (req, res) => {
    const user = await User.findById(req.params.id);

    if (!user) return res.status(404).json({ err: "bad_request", error_description: 'The customer with the given ID was not found.' });

    return res.status(200).json(user)
});


router.post('/user', async (req, res) => {
    await User.create({

        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        countryCode: req.body.countryCode,
        profile_image: req.body.profile_image

    }).then((result) => {
        return res.status(200).json({
            message: "User created",
            result: result
        });

    }).catch((err) => {
        if (err.code == 11000) return res.status(400).json({ err: "bad_request", error_description: "Email already exists" });
        else return res.status(400).json({ err: "bad_request", error_description: err })
    })
})

module.exports = router