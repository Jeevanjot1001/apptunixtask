const mongoose = require('mongoose');
const express = require('express');
const userRoutes = require('./routes/users.routes')
const roomRoutes = require('./routes/rooms.routes')
const chatRoutes = require('./routes/chats.routes')
const app = express();

app.use(express.json());

app.use('/api',userRoutes)
app.use('/api',roomRoutes)
app.use('/api',chatRoutes)

mongoose.connect('mongodb://localhost/apptunix')
    .then(()=>console.log('Connected with mongodb....'))
    .catch(err=>console.log('Not connected with mongo db ...',err))
    
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));