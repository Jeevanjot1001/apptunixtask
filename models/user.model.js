
const { Schema, Types, model} = require('mongoose');

const userSchema = new Schema ({

    email: { type:String,  unique:true },

    name: { type:String, default: ""  },

    phone: { type:String,  default: ""  },

    countryCode: { type:String,  default: ""  },

    profile_image : {
        type: String,
        default: ""
    },
}, { timestamps:true});

const User = model('User', userSchema);


module.exports=User