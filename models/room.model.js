
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let roomSchema = new Schema({
    room_name: { type: String, required: true },
    room_type: {
        type: String,
        enum: ['one_to_one', 'group'],
        required: true
    },
    room_image: { type: String ,required:false},

    room_admin_id: { type: Schema.ObjectId, ref: 'User', index: true, require: false },

    room_members_id: [{ type: Schema.ObjectId, ref: 'User' ,required:false}],
    member_count: {
        type: Number, default: 0
    }
}, { timestamps: true });

const Room = mongoose.model('Room', roomSchema);
module.exports = Room