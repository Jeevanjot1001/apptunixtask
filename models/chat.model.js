
const { Schema, model, Types } = require('mongoose');

const chatSchema = new Schema({

    message: { type: String },
    room_id: { type: Schema.ObjectId, ref: 'Room', index: true, require: false },
    sender_id: { type: Schema.ObjectId, ref: 'User', index: true, require: false },
    is_seen_by: [{ type: Schema.ObjectId, ref: 'User', required: false }],
    room_type: {
        type: String,
        enum: ['one_to_one', 'group'],
        required: true
    },
}, { timestamps: true });


const Chat = model('Chat', chatSchema);

module.exports = Chat;
